## PostgreSQL

```sh
mkdir secrets
echo -n 'postgres' > secrets/postgres-password
echo -n 'foi' > secrets/app-password
chmod --recursive u=rwX,g=rX,o= secrets/

podman build -f Dockerfile src -t postgresql:latest

podman volume create postgresql

podman run --detach \
  --read-only \
  --pod alaveteli-pod \
  --volume "${PWD}/secrets:/run/secrets:z,ro" \
  --volume postgresql:/var/lib/postgresql/data:rw \
  --log-driver=k8s-file --log-opt path=log-postgresql.json  --log-opt max-size=1mb \
  --name postgresql \
  --env POSTGRES_APP_DB=foi \
  --env POSTGRES_APP_USER=foi \
  localhost/postgresql:latest
```

### initdb.d database initialization sripts

Scripts in `/docker-entrypoint-initdb.d/` are executed when the database is created the first time by initdb.
