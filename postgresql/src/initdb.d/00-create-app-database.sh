set -o errexit
set -o nounset

DBNAME="${POSTGRES_APP_DB:-${POSTGRES_APP_USER}}"

psql -v ON_ERROR_STOP=1 --username "${POSTGRES_USER}" --dbname "${POSTGRES_DB}" \
<<EOF
CREATE ROLE ${POSTGRES_APP_USER} WITH LOGIN PASSWORD '${POSTGRES_APP_PASSWORD:-$(cat "${POSTGRES_APP_PASSWORD_FILE}")}';

CREATE DATABASE ${DBNAME}
WITH OWNER ${POSTGRES_APP_USER}
TEMPLATE template0
ENCODING 'UTF8'
LC_COLLATE 'en_US.utf8'
LC_CTYPE 'en_US.UTF-8';

GRANT ALL PRIVILEGES ON DATABASE ${DBNAME} TO ${POSTGRES_APP_USER};
EOF
