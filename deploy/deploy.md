# Deploy

```sh
ansible-playbook --ask-become-pass --extra-vars 'target_hosts=alaveteli-dev' project-alaveteli/playbook.yml

sudo su - alaveteli
export XDG_RUNTIME_DIR=/run/user/$(id -u)
export XDG_CONFIG_HOME=${HOME}/.config
systemctl --user status pod

podman volume create postgresql-data
podman volume create fluentbit-data


systemctl --user start pod
systemctl --user start postgresql
systemctl --user start fluentbit
```
