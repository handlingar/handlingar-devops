# Handlingar DevOps

## Handlingar Alaveteli new setup

[Container design](https://gitlab.com/handlingar/handlingar-devops/-/raw/main/alaveteli-containers-design.png)

### Current Solution

The existing server solution relies heavily on manual processes. Each component, such as the MTA (Postfix), POP3 (Dovecot) for email retrieval, web application hosting, and database server, requires individual installation and configuration. Upgrades are also performed manually, which can lead to inconsistencies and potential downtime.
Proposed Solution
The proposed solution aims to automate these processes and improve scalability and efficiency. This is achieved through the integration of three key components:

#### GitLab Pipelines

This component integrates with GitLab for continuous integration/continuous deployment (CI/CD) pipelines. It facilitates automated testing and deployment, and includes features such as Container Registry, Version Control, and Change Tracking.

#### Containers/Pods

The new server solution will use containers or pods to manage various services. These include MTA, POP3, Load Balancer, Web Application Server, and Database. This approach allows for better resource utilization and scalability.

#### Dynamic Environments

This component highlights the use of automation tools like Ansible for configuration management and orchestration within dynamic environments. Features include weekly scheduled builds and deploys, labeling of builds and images, and parameterized workflows.

The new solution aims to automate processes that were previously done manually, leading to a more efficient and scalable system. The use of containers/pods suggests a move towards DevOps practices with automated builds and deployments.

### Additional Information
For those involved in developing the container images and running them, it’s important to have a good understanding of the following:
1. Container Basics: Containers are lightweight, standalone, executable packages that include everything needed to run a piece of software. They are isolated from each other and bundle their own software, libraries and configuration files.
2. Docker and Kubernetes: Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. Kubernetes is an open-source platform designed to automate deploying, scaling, and operating application containers.
3. GitLab CI/CD: GitLab’s continuous integration/continuous deployment (CI/CD) features allow developers to automate the testing and deployment of their code. This ensures that the codebase always remains clean and deployable.
4. Ansible: Ansible is an open-source software provisioning, configuration management, and application-deployment tool enabling infrastructure as code.
5. Security: Security is crucial when dealing with containers. This includes understanding how to handle secrets, how to restrict container privileges, and network segmentation.
6. Troubleshooting: Participants should be equipped with troubleshooting skills to diagnose and resolve issues that may arise with the containers/pods.

