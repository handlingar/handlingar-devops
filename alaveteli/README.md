# Alaveteli

```sh
podman build -f Dockerfile src -t alaveteli:latest

podman pod create --publish 3000:3000 alavateli-pod

podman run --detach \
  --read-only \
  --pod alaveteli-pod  \
  --log-driver=k8s-file --log-opt path=log-alaveteli.json --log-opt max-size=1mb \
  --name alaveteli  \
  --env ALAVETELI_DB_USERNAME=foi \
  --env ALAVETELI_DB_PASSWORD=foi \
  --env ALAVETELI_DB_NAME=foi  \
  --env ALAVETELI_DB_HOSTNAME=postgresql \
  --volume /path/to/alavetelitheme/:/var/run/alavetelitheme:U,z,ro \
  localhost/alaveteli:latest
```
