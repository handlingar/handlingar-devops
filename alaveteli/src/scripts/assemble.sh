#!/bin/bash

set -o errexit
set -o nounset

cd "${ALAVETELI_HOME}"

export RAILS_ENV=${RAILS_ENV:-production}
export RACK_ENV=${RACK_ENV:-${RAILS_ENV}}

cat >> config/environments/${RAILS_ENV:-production} <<'EOF'
Rails.application.config.hosts = ["ENV['ALAVETELI_HOSTNAME']","ENV['ALAVETELI_HOSTNAME']:ENV['ALAVETELI_PORT']"]
EOF

if [ -n "${RUBYGEM_MIRROR:-}" ]
then
  bundle config mirror.https://rubygems.org ${RUBYGEM_MIRROR}
fi

if [ -n "${NPM_MIRROR:-}" ]
then
  npm config set registry ${NPM_MIRROR}
fi

if [ -f Gemfile ]
then
  if [ -f Gemfile.lock ]
  then
    bundle config set --local deployment "true"
  fi

  case ${RAILS_ENV} in
    development)
      bundle config set --local without 'test'
      ;;
    test)
      bundle config set --local without 'development'
      ;;
    *)
      bundle config set --local without 'development:test'
      ;;
  esac

  # The location to install the specified gems to
  bundle config set --local path ./bundle

  bundle install --retry 2

  bundle clean --verbose
fi

# git clone https://github.com/mysociety/alavetelitheme.git lib/themes/handlingar-theme
# rm -rf lib/themes/handlingar-theme/.git
