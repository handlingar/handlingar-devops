#!/bin/bash

set -o errexit
set -o nounset

chmod +x /tmp/src/scripts/*.sh

mv /tmp/src/scripts/docker-entrypoint.sh /
mv /tmp/src/scripts/{run.sh,cron.sh} /usr/bin/

mkdir /docker-entrypoint.d/ || :

touch /docker-entrypoint.d/container.envsh

mkdir -p "${ALAVETELI_HOME}"
groupadd \
  --system \
  --gid 101 \
  alaveteli
useradd \
  --system \
  --comment 'Alaveteli' \
  --no-user-group \
  --uid 101 --gid 101 \
  --home-dir "${ALAVETELI_HOME}" --no-create-home \
  --groups 0 \
  alaveteli
  # --shell /usr/sbin/nologin \

chmod --recursive g+rwX /docker-entrypoint.d/
chown root:alaveteli /docker-entrypoint.d/

DISTRIBUTION=${DISTRIBUTION:-debian}
DISTVERSION=${DISTVERSION:-bookworm}
SECURITY="${DISTVERSION}-security"
export DEBIAN_FRONTEND=noninteractive

# envsubst '${DISTVERSION}' < /tmp/src/apt-sources/debian.sources > /etc/apt/sources.list.d/debian.sources
# envsubst '${DISTVERSION}' < /tmp/src/apt-sources/debian-extra.list > /etc/apt/sources.list.d/debian-extra.list

apt-get update
apt-get install --yes --no-install-recommends \
  apt-utils \
  wget \
  git-core \
  locales \
  lockfile-progs \
  curl \
  dnsutils \
  gnupg \
  build-essential \
  make \
  ruby-full \
  bundler \
  libpq-dev \
  libicu-dev \
  zlib1g-dev \
  libmagic-dev \
  cron \
  procps \
  gettext-base \
  vim


localedef --inputfile=en_US --force --charmap=UTF-8 --alias-file=/usr/share/locale/locale.alias en_US.UTF-8


wget --quiet --directory-prefix=/etc/apt/keyrings https://debian.mysociety.org/debian.mysociety.org.gpg.key

envsubst '${DISTVERSION}' < /tmp/src/apt-sources/mysociety-debian.list > /etc/apt/sources.list.d/mysociety-debian.list
envsubst < /tmp/src/apt-sources/preferences > /etc/apt/preferences

apt-get update

apt-get install --yes --no-install-recommends \
  wkhtmltopdf-static
  # wkhtmltox

# Remove one crippling package if it's installed
apt-get remove --yes --purge apt-xapian-index >/dev/null || :

apt-get clean --yes
rm -rf \
  /var/lib/apt/lists/* \
  /usr/share/doc/* \
  /usr/share/locale/* \
  /usr/share/man/* \
  /usr/share/info/* &>/dev/null || :
rm /etc/apt/sources.list.d/debian-extra.list \
  /etc/apt/sources.list.d/mysociety-debian.list \
  /etc/apt/sources.list.d/debian.sources \
  /etc/apt/keyrings/debian.mysociety.org.gpg.key \
  /etc/apt/preferences &>/dev/null || :

git config --system advice.detachedHead false

git clone --recursive --branch master --depth 1 https://github.com/mysociety/alaveteli.git "${ALAVETELI_HOME}"

shopt -s dotglob
cp --verbose --recursive /tmp/src/alaveteli/* "${ALAVETELI_HOME}/"

chown --recursive alaveteli:root "${ALAVETELI_HOME}"
chmod --recursive ug+rwX "${ALAVETELI_HOME}"

su -c '/tmp/src/scripts/assemble.sh' alaveteli

sed -i --regex "s/BASH_ENV=.*/BASH_ENV=${ALAVETELI_HOME//\//\\/}\/tmp\/.profile/" /tmp/src/crontab/alaveteli
crontab -u alaveteli /tmp/src/crontab/alaveteli
chmod ug+s /usr/sbin/cron
ln -s /tmp/crond.pid /var/run/crond.pid
ln -s /proc/1/fd/1 /var/log/cron.log
