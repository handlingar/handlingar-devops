#!/bin/bash
set -o errexit
set -o nounset

if find /docker-entrypoint.d/ -mindepth 1 -maxdepth 1 -type f -print -quit 2>/dev/null | read v
then
  find /docker-entrypoint.d/ -follow -type f -print | sort --version-sort \
  | while read -r FILE
  do
    case "${FILE}" in
      *.envsh)
        . "${FILE}"
        ;;
      *.sh)
        "${FILE}"
        ;;
    esac
  done
fi

exec "${@}"
