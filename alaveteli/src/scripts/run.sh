#!/bin/bash

set -o errexit
set -o nounset

export RAILS_ENV=${RAILS_ENV:-production}
export RACK_ENV=${RACK_ENV:-${RAILS_ENV}}

export ALAVETELI_HOSTNAME=${HOSTNAME:-${ALAVETELI_HOSTNAME:-localhost}}

declare -p | grep ' ALAVETELI_.*' > "${ALAVETELI_HOME}/tmp/.profile"
chmod ugo+r "${ALAVETELI_HOME}/tmp/.profile"

# ln -sf /dev/stdin log/${RAILS_ENV}.${ALAVETELI_PORT}.log

# "${ALAVETELI_HOME}/script/rails-deploy-while-down"
bundle exec rake db:migrate #--trace
bundle exec rake db:seed

# "${ALAVETELI_HOME}/script/rails-deploy-before-down"
bundle exec rake submodules:check
bundle exec rake themes:install
bundle exec rake geoip:download_data
bundle exec rake assets:precompile
bundle exec rake assets:link_non_digest

# (cron.sh &)

bundle exec thin \
  --environment=${RAILS_ENV} \
  --rackup config.ru \
  --address 0.0.0.0 \
  --port ${ALAVETELI_PORT} \
  --log log/${RAILS_ENV}.log \
  --pid tmp/pids/thin.pid \
  ${ALAVETELI_DEBUG+--debug} \
  start
  # --ssl                        Enables SSL
  # --ssl-key-file PATH          Path to private key
  # --ssl-cert-file PATH         Path to certificate
  # --ssl-disable-verify         Disables (optional) client cert requests
  # --ssl-version VERSION        TLSv1, TLSv1_1, TLSv1_2
  # --ssl-cipher-list STRING     Example: HIGH:!ADH:!RC4:-MEDIUM:-LOW:-EXP:-CAMELLIA
