#!/bin/bash

{
  until /usr/sbin/cron -f &> /dev/null
  do
    echo "cron crashed, restarting..."
    kill $!
    sleep 1
  done
} > /proc/1/fd/1 2>&1
